import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OrdMenuTringgerDirective } from './ord-menu-tringger.directive';
import { OverlayModule} from '@angular/cdk/overlay';
import { OrdMenuComponent } from './ord-menu/ord-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    OrdMenuTringgerDirective,
    OrdMenuComponent
  ],
  imports: [
    BrowserModule,
    OverlayModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
