import {Directive, ElementRef, OnInit, TemplateRef, Input, HostListener, ViewContainerRef, OnDestroy, AfterViewInit} from '@angular/core';
import {ConnectionPositionPair, FlexibleConnectedPositionStrategy, Overlay, OverlayConfig, OverlayRef} from '@angular/cdk/overlay';
import {ScrollStrategy} from '@angular/cdk/overlay/scroll/scroll-strategy';
import {TemplatePortal} from '@angular/cdk/portal';
import {POSITION_MAP} from './conection-positon-pair';
import {merge, Subject, Subscription} from 'rxjs';
import {debounceTime, filter} from 'rxjs/operators';
import {ESCAPE, hasModifierKey} from '@angular/cdk/keycodes';
import {OrdMenuComponent} from './ord-menu/ord-menu.component';

@Directive({
  selector: '[ordMenuTringger]'
})
export class OrdMenuTringgerDirective implements OnInit, OnDestroy, AfterViewInit {
  @Input() ordMenuTringger: OrdMenuComponent;
  @Input() menupositons = 'rightTop';
  @Input() triggerBy: 'click' | 'hover' | null = 'click';
  overlayRef: OverlayRef;

  private menuState: 'open' | 'close' = 'close';
  private portal: TemplatePortal;
  private positons: ConnectionPositionPair[] = [
    POSITION_MAP.rightTop,
    POSITION_MAP.rightBottom,
  ];
  private subcription = Subscription.EMPTY;
  private readonly hover$ = new Subject<boolean>();
  private readonly click$ = new Subject<boolean>();

  constructor(private el: ElementRef,
              private overlay: Overlay,
              private  vcr: ViewContainerRef
  ) {
  }


  ngOnInit(): void {
    // console.log(this.el.nativeElement.)
  }

  ngAfterViewInit(): void {
    this.initialize();
  }

  ngOnDestroy(): void {
    this.subcription.unsubscribe();
  }


  @HostListener('click', ['$event'])
  onClick(event: MouseEvent) {
    if (this.ordMenuTringger) {
      // this.openMenu();
      this.click$.next(true);
    }
  }

  @HostListener('mouseenter', ['$event'])
  onMouseEnter() {
    this.hover$.next(true);
  }

  @HostListener('mouseleave', ['$event'])
  onMouseLeave() {
    this.hover$.next(false);
  }

  openMenu() {
    if (this.menuState === 'open') {
      return;
    }
    const overlayConfig = this.getOverlayConfig();
    this.setOverayPosition(overlayConfig.positionStrategy as FlexibleConnectedPositionStrategy);
    const overlayRef = this.overlay.create(overlayConfig);
    // console.log(overlayRef);
    overlayRef.attach(this.getPortal());
    this.subcribeOvelayEven(overlayRef);
    this.overlayRef = overlayRef;
    this.menuState = 'open';
  }

  closeMenu() {
    if (this.menuState === 'open') {
      this.overlayRef?.detach();
      this.menuState = 'close';
    }
  }

  private initialize() {
    const hover$ = merge(this.ordMenuTringger.visiable$, this.hover$).pipe(debounceTime(100));
    const handle$ = this.triggerBy === 'hover' ? hover$ : this.click$;
    handle$.subscribe(result => {
      if (result) {
        this.openMenu();
        return;
      }
      this.closeMenu();
    });
  }


  private getOverlayConfig(): OverlayConfig {
    console.log(' POSITION_MAP.rightTop', POSITION_MAP.rightTop);
    const positionStrategy = this.overlay.position().flexibleConnectedTo(this.el);
    return new OverlayConfig({
      positionStrategy: positionStrategy,
      minWidth: '200px',
      hasBackdrop: this.triggerBy !== 'hover',
      backdropClass: 'ord-menu-backdrop',
      panelClass: 'ord-menu-panel',
      scrollStrategy: this.overlay.scrollStrategies.reposition()
    });
  }

  private setOverayPosition(pos: FlexibleConnectedPositionStrategy) {
    pos.withPositions([...this.positons]);

  }

  private getPortal(): TemplatePortal {
    if (!this.portal || this.portal.templateRef !== this.ordMenuTringger.menuTemplate) {
      this.portal = new TemplatePortal<any>(this.ordMenuTringger.menuTemplate, this.vcr);
    }
    return this.portal;

  }

  private subcribeOvelayEven(overlayRef: OverlayRef) {
    this.subcription.unsubscribe();
    this.subcription = merge(
      overlayRef.backdropClick(),
      overlayRef.detachments(),
      overlayRef.keydownEvents().pipe(filter(event => event.keyCode === ESCAPE && !hasModifierKey(event)))
    ).subscribe(() => {
      this.closeMenu();
    });
  }
}
