import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-ord-menu',
  templateUrl: './ord-menu.component.html',
  styleUrls: ['./ord-menu.component.css']
})
export class OrdMenuComponent implements OnInit {
  public readonly visiable$ = new Subject<boolean>();
  @ViewChild(TemplateRef, {static:true}) menuTemplate: TemplateRef<any>
  constructor() {
  }

  ngOnInit(): void {
  }

}
